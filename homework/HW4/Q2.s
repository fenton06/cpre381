	.data;

	myArray: .word 1, 2, 3, 4, 5;	# The array

	.text;

	la	$s0, myArray;			# Load address of array to $s0
	li	$s1, 0;					# Load value of 0 into $s1
	
loop:
	sll		$t0, $s1, 2;		# Shift values in $s1 left 2 places (multiply by 4) and store in $t0
	add		$t0, $t0, $s0;		# Increase address stored in $s0 by value in $t0 and store in $t0
	lw		$s2, 0($t0);		# Load word at address in $t0 to $s2
	addi	$s2, $s2, 1;		# Add 1 to word stored in $s2 and store in $s2
	sw		$s2, 0($t0);		# Store contents of $s2 at address specified by $t0
	addi	$s1, $s1, 1;		# Add 1 to value stored in $s1 and store in $s1
	slti	$t1, $s1, 5;		# Set $t1 to 1 if value of $s1 < 5
	bne		$t1, $zero, loop;	# If $t1 != 0, got back to loop
	
	.end;
