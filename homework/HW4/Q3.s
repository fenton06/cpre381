comp:	
	lw	$t0, $a0		# Load A into $t0
	lw	$t1, $a1		# Load B into $t1

	add	$t3, $t0, $t1	# A + B, store in $t3

	slt	$t4, $t3, $t0	# Check if A + B < A, set $t4 to 1 if true
	slt	$t5, $t3, $t1	# Check if A + B < B, set $t5 to 1 if true

	and	$v0, $t4, $t5	# $t4 && $t5, store result in $v0

	j	$ra				# Return to caller