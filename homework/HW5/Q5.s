f2c:
	lwc1	$f16, const5($gp)	# $f16 = 5.0 (5.0 in memory)
	lwc1	$f18, const9($gp)	# $f18 = 9.0 (9.0 in memory)
	div.s	$f16, $f16, $f18	# $f16 = 5.0 / 9.0
	lwc1	$f18, const32($gp)	# $f18 = 32.0
	sub.s	$f18, $f12, $f18	# $f18 = fahr – 32.0
	mul.s	$f0, $f16, $f18		# $f0 = (5/9)*(fahr – 32.0)
	blez	$f0, LTZ			# Branch to LTZ if $f0 <= 0
	jr		$ra					# Return
LTZ:
	lwc1	$f0, ZERO($0)		# Set $f0 to 0
	jr		$ra					# Return 0