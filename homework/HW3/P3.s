.data

msg1: .asciiz "Enter Fibonocci number index (starts at 0): "
msg2: .asciiz "Desired number: "

nl: .asciiz "\n";

.text
.globl main

main:
	move	$a1, $zero;
	li		$a2, 1;
	li		$v0, 4;
	la		$a0, msg1;		# User prompt
	syscall
	li		$v0, 5;
	syscall					# Read input
	slt		$t0, $v0, $zero;          
	beq		$t0, 1, endit;	# End if user enters 1
	move	$a1, $v0;		# Move input value to $a1
	jal		fib;			# Call fib
	move	$v1, $a0;		# Temp storage of answer now in $a0 to $v1
	li		$v0, 4;
	la		$a0, msg2;		# Fib number
	syscall;
	li		$v0, 1;			# Answer
	move	$a0, $v1;		# Bring back temp Storage of answer from $v1
	syscall;
	j		endit;
	
fib:
	subu	$sp, $sp, 32;	# Set up stack frame
	sw		$ra, 20($sp);
	sw		$fp, 16($sp);
	addiu	$fp, $sp, 28;
	sw		$a1, 0($fp);
	beq		$a1, 1, ret1;
	beq		$a1, 0, ret1;
	lw		$a1, 0($fp);
	subu	$a1, $a1, 1;	# Subtract 1
	jal		fib;			# Calculate fib(n-1)
	move	$a2, $a0;		# Save in a2
	sw		$a2, 4($fp);
	lw		$a1, 0($fp);
	subu	$a1, $a1, 2;	# Subtract 2
	jal		fib;			# Calculate fib(n-2)
	lw		$a2, 4($fp);
	add		$a0, $a2, $a0;
	j		done;
	
ret1:
	li		$a0, 1;

done:	
	lw		$ra, 20($sp);	# Remove stack frame
	lw		$fp, 16($sp);
	addiu	$sp, $sp, 32;
	jr		$ra;

endit:                    
	li		$v0, 10;
	syscall;
	