-------------------------------------------------------------------------
-- Arun Sondhi and Benjamin Fenton
-------------------------------------------------------------------------
-- RegisterFile.vhd
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.Array2D.all;			-- 2D array

entity RegisterFile is
	generic(N: integer := 32);
	port(
		i_CLK	: in std_logic;							-- Clock
		i_RST	: in std_logic;							-- Reset
		rs		: in std_logic_vector(4 downto 0);		-- Read Register 1
		rt		: in std_logic_vector(4 downto 0);		-- Read Register 2
		rd		: in std_logic_vector(4 downto 0);		-- Write register
		WE		: in std_logic;							-- Write Enable
		Data	: in std_logic_vector(N-1 downto 0);	-- Data to write
		o_rs	: out std_logic_vector(N-1 downto 0);	-- Read Data 1
		o_rt	: out std_logic_vector(N-1 downto 0)	-- Read Data 2
	);
end RegisterFile;

architecture structural of RegisterFile is

	signal s_rd : std_logic_vector(31 downto 0);
	signal WEandRD : std_logic_vector(31 downto 0);
	signal o_Q : array32(31 downto 0);
	
	component nRegister
		generic(N: integer := 32);
		port(
			i_CLK	: in std_logic;							-- Clock input
			i_RST	: in std_logic;							-- Reset input
			i_WE	: in std_logic;							-- Write enable input
			i_D		: in std_logic_vector(N-1 downto 0);	-- Data value input
			o_Q		: out std_logic_vector(N-1 downto 0)	-- Data value output
		);
	end component;

	component Decoder5_32
		port(
			i_A  : in std_logic_vector(4 downto 0);
			o_B  : out std_logic_vector(31 downto 0)
		);
	end component;

	component Mux32_1
		port(
			i_A  : in array32;
			i_S  : in std_logic_vector(4 downto 0);
			o_F  : out std_logic_vector(31 downto 0)
		);
	end component;
	
	begin

		-- Create a multiplexed input to the FF based on i_WE
		write_rd: Decoder5_32
			port map(
				rd,
				s_rd
			);
		
		read_rs: Mux32_1
			port map(
				o_Q,
				rs,
				o_rs
			);
		
		read_rt: Mux32_1
			port map(
				o_Q,
				rt,
				o_rt
			);
		
		reg_0: nRegister
			port map(
				i_CLK => i_CLK,
				i_RST => i_RST,
				i_WE => '0',
				i_D => Data,
				o_Q => o_Q(0)
			);
		
		G1: for i in 1 to 31 generate
			WEandRD(i) <= WE and s_rd(i);
			
			reg_i: nRegister
			port map(
				i_CLK => i_CLK,
				i_RST => i_RST,
				i_WE => WEandRD(i),
				i_D => Data,
				o_Q => o_Q(i)
			);   
		end generate;
		
end structural;
