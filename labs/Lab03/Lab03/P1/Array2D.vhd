-------------------------------------------------------------------------
-- Arun Sondhi and Benjamin Fenton
-------------------------------------------------------------------------
-- Array2D.vhd
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

package Array2D is
	type array32 is array(natural range <>) of std_logic_vector(31 downto 0);
end Array2D;
