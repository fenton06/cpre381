-------------------------------------------------------------------------
-- Arun Sondhi and Benjamin Fenton
-------------------------------------------------------------------------
-- Mux32_1.vhd
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;		-- Needed for to_integer
use work.Array2D.all;			-- 2D array

entity Mux32_1 is
	port(
		i_A  : in array32;	-- In data
		i_S  : in std_logic_vector(4 downto 0);		-- Select
		o_F  : out std_logic_vector(31 downto 0)	-- Output
	);
end Mux32_1;

architecture dataflow of Mux32_1 is
	
	begin
		
		o_F <= i_A(to_integer(unsigned (i_S)));
		
end dataflow;
