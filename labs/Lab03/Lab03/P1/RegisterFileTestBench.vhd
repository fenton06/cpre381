-------------------------------------------------------------------------
-- Arun Sondhi and Benjamin Fenton
-------------------------------------------------------------------------
-- RegisterFileTestBench.vhd
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_signed.all;
use work.Array2D.all;			-- 2D array

entity RegisterFileTestBench is
	generic(gCLK_HPER   : time := 50 ns);
end RegisterFileTestBench;

architecture behavior of RegisterFileTestBench is
  
	-- Calculate the clock period as twice the half-period
	constant cCLK_PER  : time := gCLK_HPER * 2;

	component RegisterFile
		generic(N: integer := 32);
		port(
			i_CLK	: in std_logic;							-- Clock
			i_RST	: in std_logic;							-- Reset
			rs		: in std_logic_vector(4 downto 0);		-- Read Register 1
			rt		: in std_logic_vector(4 downto 0);		-- Read Register 2
			rd		: in std_logic_vector(4 downto 0);		-- Write register
			WE		: in std_logic;							-- Write Enable
			Data	: in std_logic_vector(N-1 downto 0);	-- Data to write
			o_rs	: out std_logic_vector(N-1 downto 0);	-- Read Data 1
			o_rt	: out std_logic_vector(N-1 downto 0)	-- Read Data 2
		);
	end component;

	-- Temporary signals
	signal s_CLK : std_logic;
	signal s_RST : std_logic;
	signal s_rs : std_logic_vector(4 downto 0) := "00000";
	signal s_rt : std_logic_vector(4 downto 0) := "00000";
	signal s_rd : std_logic_vector(4 downto 0) := "00000";
	signal s_WE : std_logic := '0';
	signal rs : std_logic_vector(31 downto 0) := x"00000000";
	signal rt : std_logic_vector(31 downto 0) := x"00000000";
	signal w_data : std_logic_vector(31 downto 0) := x"00000000";

	begin

		g_RegFile: RegisterFile
			port map(
				s_CLK,
				s_RST,
				s_rs,
				s_rt,
				s_rd,
				s_WE,
				w_data,
				rs,
				rt
			);

		-- This process sets the clock value (low for gCLK_HPER, then high
		-- for gCLK_HPER). Absent a "wait" command, processes restart 
		-- at the beginning once they have reached the final statement.
		p_clk: process
			begin
			s_CLK <= '0';
			wait for gCLK_HPER;
			s_CLK <= '1';
			wait for gCLK_HPER;
		end process;

		-- Testbench process  
		p_tb: process
			begin
			
			-- Reset registers
			s_RST <= '1';
			wait for cCLK_PER;
			
			-- Write data to R1 (Writing 4 -> b"0100")
			s_RST <= '0';
			w_data <= x"00000004";
			s_rd <= "00001";
			s_WE <= '1';
			wait for cCLK_PER;
			
			-- Write data to R2 (Writing 6 -> b"0110")
			w_data <= x"00000006";
			s_rd <= "00010";
			s_WE <= '1';
			wait for cCLK_PER;
			
			-- Read data from R1 and R2
			s_rs <= "00001";
			s_rt <= "00010";
			s_WE <= '0';
			wait for cCLK_PER;
			
			-- Write R1 + R2 to R3 (6 + 4 = 10 -> b"1010")
			w_data <= rs + rt;
			s_rd <= "00011";
			s_WE <= '1';
			wait for cCLK_PER;
			
			-- Read data from R3 and R0
			s_rs <= "00011";
			s_rt <= "00000";
			s_WE <= '0';
			wait for cCLK_PER;
			
			-- Attempt to write data to R0
			w_data <= x"00000006";
			s_rd <= "00000";
			s_WE <= '1';
			wait for cCLK_PER;
			
			-- Read data from R3 and R0 (R0 should be 0!!!)
			s_rs <= "00011";
			s_rt <= "00000";
			s_WE <= '0';
			wait for cCLK_PER;

			-- Write data to R1 (Writing 6 -> b"0110")
			s_RST <= '0';
			w_data <= x"00000006";
			s_rd <= "00001";
			s_WE <= '0';
			wait for cCLK_PER;

			-- Read data from R3 and R0 (R0 should be 0!!!)
			s_rs <= "00011";
			s_rt <= "00000";
			s_WE <= '0';
			wait for cCLK_PER;

			wait;
		end process;

end behavior;
