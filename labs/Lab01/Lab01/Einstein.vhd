-------------------------------------------------------------------------
-- Arun Sondhi and Ben Fenton
-------------------------------------------------------------------------


-- Einstein.vhd
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;


entity Einstein is

  port(iCLK             : in std_logic;
       iM 		            : in integer;
       oY 		            : out integer);

end Einstein;

architecture structure of Einstein is

  component Multiplier
    port(iCLK           : in std_logic;
         iA             : in integer;
         iB             : in integer;
         oC             : out integer);
  end component;

  -- Arbitrary constants for C. No need to change these.
  constant cC : integer := 9487;

  -- Signals to store c*c
  signal sVALUE_cxc: integer;

begin

  
  ---------------------------------------------------------------------------
  -- Level 1: Calculate c*c, m*c
  ---------------------------------------------------------------------------
  g_Mult1: Multiplier
    port MAP(iCLK             => iCLK,
             iA               => cC,
             iB               => cC,
             oC               => sVALUE_cxc);

  g_Mult2: Multiplier
    port MAP(iCLK             => iCLK,
             iA               => sVALUE_cxc,
             iB               => iM,
             oC               => oY);
    
  
end structure;
