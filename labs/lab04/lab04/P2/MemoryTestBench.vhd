-------------------------------------------------------------------------
-- Arun Sondhi and Benjamin Fenton
-------------------------------------------------------------------------
-- MemoryTestBench.vhd
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity MemoryTestBench is
	generic(gCLK_HPER   : time := 50 ns);
	port(
		o_q : out std_logic_vector(31 downto 0)
	);
end MemoryTestBench;

architecture structure of MemoryTestBench is
	
	-- Calculate the clock period as twice the half-period
	constant cCLK_PER  : time := gCLK_HPER * 2;
	
	component mem is
		generic(depth_exp_of_2 : integer := 12;
		mif_filename	: string := "dmem.mif");

		port(
			address	: in std_logic_vector(depth_exp_of_2 - 1 downto 0) := (Others => '0');
			byteena	: in std_logic_vector(3 downto 0) := (others =>'1');
			clock	: in std_logic := '1';
			data	: in std_logic_vector(31 downto 0) := (others => '0');
			wren	: in std_logic := '0';
			q		: out std_logic_vector(31 downto 0)
		);
	end component;

	-- Temp signals
	signal s_addr	: std_logic_vector(11 downto 0);
	signal s_CLK	: std_logic;
	signal s_wren	: std_logic;
	signal s_data	: std_logic_vector(31 downto 0);
	signal s_q		: std_logic_vector(31 downto 0);


	begin
	
		dmem : mem
		generic map(depth_exp_of_2 => 12)
		port map(
			address => s_addr,
			byteena => "1111",
			clock   => s_CLK,
			data    => s_data,
			wren    => s_wren,
			q       => s_q
		);
	
	-- This process sets the clock value (low for gCLK_HPER, then high
	-- for gCLK_HPER). Absent a "wait" command, processes restart 
	-- at the beginning once they have reached the final statement.
	P_CLK: process
	
		begin
		
		s_CLK <= '1';
		wait for gCLK_HPER;
		s_CLK <= '0';
		wait for gCLK_HPER;
		
	end process;
	

	process
	
		begin
		
		o_q <= s_q;
		
		-- Read data in address x"000"
		s_wren <= '0';
		s_addr <= x"000";
		wait for cCLK_PER;
		
		-- Write data to address x"100"
		s_wren <= '1';
		s_addr <= x"100";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Read data from address x"100"
		s_wren <= '0';
		s_addr <= x"100";
		o_q <= s_q;
		wait for cCLK_PER;
		
		-- Read data in address x"001"
		s_wren <= '0';
		s_addr <= x"001";
		wait for cCLK_PER;
		
		-- Write data to address x"101"
		s_wren <= '1';
		s_addr <= x"101";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Read data from address x"101"
		s_wren <= '0';
		s_addr <= x"101";
		o_q <= s_q;
		wait for cCLK_PER;
		
		-- Read data in address x"002"
		s_wren <= '0';
		s_addr <= x"002";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Write data to address x"102"
		s_wren <= '1';
		s_addr <= x"102";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Read data from address x"102"
		s_wren <= '0';
		s_addr <= x"102";
		o_q <= s_q;
		wait for cCLK_PER;
		
		-- Read data in address x"003"
		s_wren <= '0';
		s_addr <= x"003";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Write data to address x"103"
		s_wren <= '1';
		s_addr <= x"103";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Read data from address x"103"
		s_wren <= '0';
		s_addr <= x"103";
		o_q <= s_q;
		wait for cCLK_PER;
		
		-- Read data in address x"004"
		s_wren <= '0';
		s_addr <= x"004";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Write data to address x"104"
		s_wren <= '1';
		s_addr <= x"104";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Read data from address x"104"
		s_wren <= '0';
		s_addr <= x"104";
		o_q <= s_q;
		wait for cCLK_PER;
		
		-- Read data in address x"005"
		s_wren <= '0';
		s_addr <= x"005";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Write data to address x"105"
		s_wren <= '1';
		s_addr <= x"105";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Read data from address x"105"
		s_wren <= '0';
		s_addr <= x"105";
		o_q <= s_q;
		wait for cCLK_PER;
		
		-- Read data in address x"006"
		s_wren <= '0';
		s_addr <= x"006";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Write data to address x"106"
		s_wren <= '1';
		s_addr <= x"106";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Read data from address x"106"
		s_wren <= '0';
		s_addr <= x"106";
		o_q <= s_q;
		wait for cCLK_PER;
		
		-- Read data in address x"007"
		s_wren <= '0';
		s_addr <= x"007";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Write data to address x"107"
		s_wren <= '1';
		s_addr <= x"107";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Read data from address x"107"
		s_wren <= '0';
		s_addr <= x"107";
		o_q <= s_q;
		wait for cCLK_PER;
		
		-- Read data in address x"008"
		s_wren <= '0';
		s_addr <= x"008";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Write data to address x"108"
		s_wren <= '1';
		s_addr <= x"108";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Read data from address x"108"
		s_wren <= '0';
		s_addr <= x"108";
		o_q <= s_q;
		wait for cCLK_PER;
		
		-- Read data in address x"009"
		s_wren <= '0';
		s_addr <= x"009";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Write data to address x"109"
		s_wren <= '1';
		s_addr <= x"109";
		s_data <= s_q;
		wait for cCLK_PER;
		
		-- Read data from address x"109"
		s_wren <= '0';
		s_addr <= x"109";
		o_q <= s_q;
		wait for cCLK_PER;
		
		wait;
	end process;

end structure;
