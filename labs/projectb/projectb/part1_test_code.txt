DEPTH=1024; 	% Memory depth and width are required 
		% DEPTH is the number of addresses 
WIDTH = 32; 	% WIDTH is the number of bits of data per word
		% DEPTH and WIDTH should be entered as decimal numbers

ADDRESS_RADIX = DEC; 	% Address and value radixes are required
DATA_RADIX = BIN; 	% Enter BIN, DEC, HEX, OCT, or UNS; unless
			% otherwise specified, radixes = HEX

-- Specify values for addresses, which can be single address or range
-- SOME THINGS TO CONSIDER
-- 1) This memory is word-addressable, versus the MIPS conceptual byte-addressable memory.
--    This means that address 1 corresponds to word 1, address 2 corresponds to word 2, etc.
--    Since MIPS will be generating byte addresses, you will have to create some (very simple)
--    logic in your VHDL wrapper to deal with byte/word offsets. 
-- 2) The "single address" notation seems to be least likely to cause confusion so that is what
--    I recommend. 
-- 3) The values need to be in 32-bit hex form (i.e. don't use F when you really mean 0000000F).

CONTENT
BEGIN

0 : 00100000000001000000000000010000;
1 : 00100000000001010000000000100000;
2 : 00100000000001100000000000000100;
3 : 00100000000001110000000000001000;
4 : 00000000000001000100000010000000;
5 : 00000000100001010100100000100000;
6 : 10001100110010100000000000001000;
7 : 10101100111010110000000000000100;
[8..255] : 00000000000000000000000000000000; 

END;
