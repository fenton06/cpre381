-------------------------------------------------------------------------
-- Arun Sondhi and Benjamin Fenton
-------------------------------------------------------------------------
-- nMux2TestBench.vhd
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity nMux2TestBench is
	generic(S : integer := 8);
	port(
		o_Dataflow	: out std_logic_vector(S-1 downto 0);
		o_structure	: out std_logic_vector(S-1 downto 0)
	);
end nMux2TestBench;

architecture structure of nMux2TestBench is

	component nMux2Structural
		generic(N : integer := 32);
		port(
			i_A	: in std_logic_vector(N-1 downto 0);
			i_B	: in std_logic_vector(N-1 downto 0);
			i_S	: in std_logic;
			o_F	: out std_logic_vector(N-1 downto 0)
		);
	end component;

	component nMux2Dataflow
		generic(N : integer := 32);
		port(
			i_A	: in std_logic_vector(N-1 downto 0);
			i_B	: in std_logic_vector(N-1 downto 0);
			i_S	: in std_logic;
			o_F	: out std_logic_vector(N-1 downto 0)
		);
	end component;
	
	signal i_tA : std_logic_vector(S-1 downto 0);
	signal i_tB : std_logic_vector(S-1 downto 0);
	signal i_tS : std_logic;

	begin
	
	g_nMux2Structural: nMux2Structural
		generic map(N => 8)
		port map(
			i_A  => i_tA,
			i_B  => i_tB,
			i_S  => i_tS,
			o_F  => o_Structure
		);
			  
	g_nMux2Dataflow: nMux2Dataflow
		generic map(N => 8)
		port map(
			i_A  => i_tA,
			i_B  => i_tB,
			i_S  => i_tS,
			o_F  => o_Dataflow
		);
			  
	process

		begin

		I_tA <= x"FF";
		I_tB <= x"00";
		I_tS <= '1';
		wait for 100 ns;

		I_tS <= '0';
		wait for 100 ns;

		I_tA <= x"00";
		I_tB <= x"FF";      
		I_tS <= '1';
		wait for 100 ns;      

		I_tS <= '0';
		wait for 100 ns;

	end process;
	
end structure;