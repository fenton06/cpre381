-------------------------------------------------------------------------
-- Arun Sondhi and Benjamin Fenton
-------------------------------------------------------------------------
-- nMux2Dataflow.vhd
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity nMux2Dataflow is
	generic(N : integer := 14);
	port(
		i_A	: in std_logic_vector(N-1 downto 0);
		i_B	: in std_logic_vector(N-1 downto 0);
		i_S	: in std_logic;
		o_F	: out std_logic_vector(N-1 downto 0)
	);
end nMux2Dataflow;

architecture dataflow of nMux2Dataflow is
	
	-- Signals to store A*x, B*x
	signal ts		: std_logic_vector(N-1 downto 0);
	signal tNotS	: std_logic_vector(N-1 downto 0);
	signal tAndA	: std_logic_vector(N-1 downto 0);
	signal tAndB	: std_logic_vector(N-1 downto 0);
	
	begin
	
	G1: for i in 0 to N-1 generate
		tS(i)	<= i_S;
	end generate;
	
	tNotS	<=       not tS;
	tAndA	<= i_A   and tNotS;
	tAndB	<= i_B   and ts;
	o_F		<= tAndA or  tAndB;
	
end dataflow;