-------------------------------------------------------------------------
-- Arun Sondhi and Benjamin Fenton
-------------------------------------------------------------------------
-- nMux2Structural.vhd
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity nMux2Structural is
	generic(N : integer := 14);
	port(
		i_A	: in std_logic_vector(N-1 downto 0);
		i_B	: in std_logic_vector(N-1 downto 0);
		i_S	: in std_logic;
		o_F	: out std_logic_vector(N-1 downto 0)
	);
end nMux2Structural;

architecture structure of nMux2Structural is

	component and2
		port(
			i_A	: in std_logic;
			i_B	: in std_logic;
			o_F	: out std_logic
		);
	end component;

	component or2
		port(
			i_A	: in std_logic;
			i_B	: in std_logic;
			o_F	: out std_logic
		);
	end component;

	component inv
		port(
			i_A	: in std_logic;
			o_F	: out std_logic
		);
	end component;

	-- Signals to store A*x, B*x
	signal tNotS : std_logic_vector(N-1 downto 0);
	signal tAndA : std_logic_vector(N-1 downto 0);
	signal tAndB : std_logic_vector(N-1 downto 0);

	begin

	G1: for i in 0 to N-1 generate
	
		inv_i1: inv
			port map(
				i_S,
				tNotS(i)
			);
		
		and_i1: and2
			port map(
				tNotS(i),
				i_A(i),
				tAndA(i)
			);
		
		and_i2: and2
			port map(
				i_S,
				i_B(i),
				tAndB(i)
			);
		
		or_i1: or2
			port map(
				tAndA(i),
				tAndB(i),
				o_F(i)
			);
		
	end generate;
  
end structure;
