-------------------------------------------------------------------------
-- Arun Sondhi and Benjamin Fenton
-------------------------------------------------------------------------
-- AdderSubtracterTestbench.vhd
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity AdderSubtracterTestbench is
	generic(N	: integer := 32);
	port(
		o_Carry	: out std_logic;
		o_Sum	: out std_logic_vector(N-1 downto 0)
	);
end AdderSubtracterTestbench;

architecture structure of AdderSubtracterTestbench is
	
	component AdderSubtracter is
		generic(S : integer := 32);
		port(
			i_Operand1	: in std_logic_vector(S-1 downto 0);
			i_Operand2	: in std_logic_vector(S-1 downto 0);
			i_Carryin	: in std_logic;
			i_Select	: in std_logic;
			o_Carryout	: out std_logic;
			o_Sum		: out std_logic_vector(S-1 downto 0)
		);
	end component;
	
	signal i_tA	: std_logic_vector(N-1 downto 0);
	signal i_tB	: std_logic_vector(N-1 downto 0);
	signal i_tS	: std_logic;
	signal i_tC	: std_logic;
	
	begin
	
	g_Adder: AdderSubtracter
		generic map(S => N)
		port map(
			i_Operand1	=> i_tA,
			i_Operand2	=> i_tB,
			i_Carryin	=> i_tC,
			i_Select	=> i_tS,
			o_Carryout	=> o_Carry,
			o_Sum		=> o_Sum
		);
	
	process
	
		begin
		
		i_tC	<= '0';
		I_tA	<= x"00000000";
		I_tB	<= x"00000001";
		I_tS	<= '0';
		wait for 100 ns;
		
		I_tS	<= '1';
		wait for 100 ns;
		
		I_tA	<= x"00000001";
		I_tB	<= x"00000001";      
		I_tS	<= '0';
		wait for 100 ns;
		
		I_tS	<= '1';
		wait for 100 ns;
		
		I_tA	<= x"FF000000";
		I_tB	<= x"01000000";      
		I_tS	<= '0';
		wait for 100 ns;
		
		I_tS	<= '1';
		wait for 100 ns;
		
		I_tA	<= x"AA000000";
		I_tB	<= x"A0000000";      
		I_tS	<= '0';
		wait for 100 ns;
		
		I_tS	<= '1';
		wait for 100 ns;
	
	end process;

end structure;
