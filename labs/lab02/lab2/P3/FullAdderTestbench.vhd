-------------------------------------------------------------------------
-- Arun Sondhi and Benjamin Fenton
-------------------------------------------------------------------------
-- FullAdderTestbench.vhd
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity FullAdderTestbench is
	generic(S : integer := 32);
	port(
		o_Structure	: out std_logic_vector(S-1 downto 0);
		c_Structure	: out std_logic;
		o_Dataflow	: out std_logic_vector(S-1 downto 0);
		c_Dataflow	: out std_logic
	);
end FullAdderTestbench;

architecture structure of FullAdderTestbench is

	component nFullAdderStructural is
		generic(N : integer := 32);
		port(
			i_A	: in std_logic_vector(N-1 downto 0);
			i_B	: in std_logic_vector(N-1 downto 0);
			i_C	: in std_logic;
			o_S	: out std_logic_vector(N-1 downto 0);
			o_C	: out std_logic
		);
	end component;

	component nFullAdderDataflow is
		generic(N : integer := 32);
		port(
			i_A	: in std_logic_vector(N-1 downto 0);
			i_B	: in std_logic_vector(N-1 downto 0);
			i_C	: in std_logic;
			o_S	: out std_logic_vector(N-1 downto 0);
			o_C	: out std_logic
		);
	end component;

	signal i_tA	: std_logic_vector(S-1 downto 0);
	signal i_tB	: std_logic_vector(S-1 downto 0);
	signal i_tC	: std_logic;
	
	begin
	
	g_structure: nFullAdderStructural
		generic map(N => 32)
		port map(
			i_A	=> i_tA,
			i_B	=> i_tB,
			i_C	=> i_tC,
			o_S	=> o_Structure,
			o_C	=> c_Structure
		);
	
	g_dataflow: nFullAdderDataFlow
		generic map(N => 32)
		port map(
			i_A	=> i_tA,
			i_B	=> i_tB,
			i_C	=> i_tC,
			o_S	=> o_Dataflow,
			o_C	=> c_Dataflow
		);
	
	process
	
		begin
		
		I_tA <= x"10000000";
		I_tB <= x"00000000";
		I_tC <= '0';
		wait for 100 ns;

		I_tC <= '1';
		wait for 100 ns;

		I_tA <= x"F0000000";
		I_tB <= x"F0000000";      
		I_tC <= '0';
		wait for 100 ns;

		I_tC <= '1';
		wait for 100 ns;

	end process;

end structure;
