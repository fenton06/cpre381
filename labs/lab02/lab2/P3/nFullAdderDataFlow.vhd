-------------------------------------------------------------------------
-- Arun Sondhi and Benjamin Fenton
-------------------------------------------------------------------------
-- nFullAdderDataflow.vhd
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity nFullAdderDataflow is
	generic(N: integer := 32);
	port(
		i_A	: in std_logic_vector(N-1 downto 0);
		i_B	: in std_logic_vector(N-1 downto 0);
		i_C	: in std_logic;
		o_S	: out std_logic_vector(N-1 downto 0);
		o_C	: out std_logic
	);
end nFullAdderDataflow;

architecture dataflow of nFullAdderDataflow is

	signal tC		: std_logic_vector(N downto 0);
	signal AXorB	: std_logic_vector(N-1 downto 0);
	signal AAndB	: std_logic_vector(N-1 downto 0);
	signal tAndC	: std_logic_vector(N-1 downto 0);

	begin
	
	tC(0) <= i_C;

	G1: for i in 0 to N-1 generate  
	
		AXorB(i)	<= i_A(i)	xor	i_B(i);
		AAndB(i)	<= i_A(i)	and	i_B(i);
		tAndC(i)	<= AXorB(i)	and	tC(i);    
		tC(i+1)		<= AAndB(i)	or	tAndC(i);
		o_S(i)		<= AXorB(i)	xor	tC(i);
	
	end generate;

	o_C	<= tC(N);

end dataflow;
