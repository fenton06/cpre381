configuration behavioral of tb_cpu1 is
	for behavior
		for cpu1 : cpu
			for scp
				for REGS : regfile use
					entity work.regfile(behavior);
				end for;
				for CALC_PC_PLUS_4, CALC_BRANCH_TARGET : adder use
					entity work.adder(behavior);
				end for;
			end for;
		end for;
	end for;
end behavioral;

configuration structural of tb_cpu1 is
	for behavior
		for cpu1 : cpu
			for scp
				for REGS : regfile use
					entity work.regfile(structural);
				end for;
				for CALC_PC_PLUS_4, CALC_BRANCH_TARGET : adder use
					entity work.adder(structural);
				end for;
			end for;
		end for;
	end for;
end structural;
