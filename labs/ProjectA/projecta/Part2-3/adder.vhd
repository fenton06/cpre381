 -- adder.vhd
 --
 -- The adders used for calculating PC-plus-4 and branch targets
 -- CprE 381
 --
 -- Zhao Zhang, Fall 2013
 --

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.mips32.all;

entity adder is
	generic (DELAY : time := 19.0 ns;	N : integer := 32);
	port (
		src1	: in  m32_word;
		src2	: in  m32_word;
		result	: out m32_word
	);
end entity;

-- Behavior modeling of ADDER
architecture behavior of adder is

	begin
	
	ADD : process (src1, src2)
		variable a	: integer;
		variable b	: integer;
		variable c	: integer;
		
		begin
		
		-- Pre-calculate
		a := to_integer(signed(src1));
		b := to_integer(signed(src2));
		c := a + b;

		-- Convert integer to 32-bit signal
		result <= std_logic_vector(to_signed(c, result'length)) after DELAY;
	
	end process;
end behavior;

-- Structural modeling of ADDER
architecture structural of adder is

	component FullAdderStructural is
		port(
			i_OperandA	: in std_logic;
			i_OperandB	: in std_logic;
			i_Carry		: in std_logic;
			o_Sum		: out std_logic;
			o_Carry		: out std_logic
		);
	end component;

	signal temp_Carryin	: std_logic_vector(N downto 0);

	begin
	
--	temp_Carryin(0)	<=	i_C;
	temp_Carryin(0)	<=	'0';

	G1: for i in 0 to N-1 generate
		adder_i: FullAdderStructural
			port map(
				i_OperandA	=>	src1(i),
				i_OperandB	=>	src2(i),
				i_Carry		=>	temp_Carryin(i),
				o_Sum		=>	result(i),
				o_Carry		=>	temp_Carryin(i+1)
			);
	end generate;

--	o_C	<=	temp_Carryin(N);

end structural;
