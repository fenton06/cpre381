-------------------------------------------------------------------------
-- Arun Sondhi and Benjamin Fenton
-------------------------------------------------------------------------
-- FullAdderStructural.vhd
-------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.mips32.all;

entity FullAdderStructural is  
	port(
		i_OperandA : in std_logic;
		i_OperandB : in std_logic;
		i_Carry    : in std_logic;
		o_Sum      : out std_logic;
		o_Carry    : out std_logic
	);
end FullAdderStructural;

architecture structure of FullAdderStructural is

	component and2 is
		port(
			src1          : in m32_1bit;
			src2          : in m32_1bit;
			result          : out m32_1bit
		);
	end component;

	component or2 is
		port(
			src1          : in m32_1bit;
			src2          : in m32_1bit;
			result          : out m32_1bit
		);
	end component;

	component xor2 is
		port(
			src1          : in m32_1bit;
			src2          : in m32_1bit;
			result          : out m32_1bit
		);
	end component;
	
	signal output_Xor1	: std_logic;
	signal output_And1	: std_logic;
	signal output_And2	: std_logic;

	begin

	g_And2_1: and2
		port map(
			src1 => i_OperandA,
			src2 => i_OperandB,
			result => output_And1
		);

	g_And2_2: and2
		port map(
			src1 => output_Xor1,
			src2 => i_Carry,
			result => output_And2
		);

	g_Xor2_1: xor2
		port map(
			src1 => i_OperandA,
			src2 => i_OperandB,
			result => output_Xor1
		);

	g_Xor2_2: xor2
		port map(
			src1 => output_Xor1,
			src2 => i_Carry,
			result => o_Sum
		);

	g_Or2: or2
		port map(
			src1 => output_And1,
			src2 => output_And2,
			result => o_Carry
		);
	
end structure;
